FROM node:10-alpine
RUN apk add --no-cache python build-base # build base includes g++ and gcc and Make
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

# create app directory
WORKDIR /home/node/app
COPY package*.json ./
USER node
RUN npm install
COPY --chown=node:node . .
RUN npm run build

# Runtime variables
ENV PORT=8080

EXPOSE 8080
CMD [ "npm", "run", "start"]
