const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const trunks = require('trunks-log');

const app = express();

// Set a global for the app basedir
global.__basedir = __dirname;

app.use(cors());

//App.js Logging
const logs = new trunks('', 'yellow', '');

// Establish routes for web/api
const { apiRoutes } = require('./src/routes/index');
const { webRoutes } = require('./src/routes/index');

// Use native ES6 Promises since mongoose's are deprecated.
mongoose.Promise = global.Promise;

//Are we using MongoDB?
if (process.env.MONGO_URI) {
  // Use native ES6 Promises since mongoose's are deprecated.
  mongoose.Promise = global.Promise;

  // Connect to the database
  mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true
  });

  // Fail on connection error.
  mongoose.connection.on('error', error => {
    throw error;
  });
} else {
  logs.success('Loaded without database connection!');
}


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/public/', express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', apiRoutes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
});

module.exports = app;


logs.success('App running on http://localhost:{}', process.env.PORT);