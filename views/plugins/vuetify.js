import 'material-design-icons-iconfont/dist/material-design-icons.css';
import Vue from 'vue';
import Vuetify from 'vuetify/lib';

const opts = {
    icons: {
        iconfont: 'md',
    },
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: '#2e2e2e', //Main color
                primaryText: '#FFFFFF', //Color for text on primary
                secondary: '#90CAF9', //Color for active nav
                lightText: '#FFFFFF'
            },
            light: {
                primary: '#ffc107', //Main color
                primaryText: '#000', //Color for text on primary
                secondary: '#90CAF9', //Color for active nav
                lightText: '#000'
            },
        }
    }
};

Vue.use(Vuetify);

export default new Vuetify(opts);