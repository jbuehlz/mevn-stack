import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import 'vuetify/dist/vuetify.min.css';

//import socketio from 'socket.io';
//import VueSocketIO from 'vue-socket.io';

Vue.config.productionTip = false;

import VueWorker from 'vue-worker';
Vue.use(VueWorker);

//export const SocketInstance = socketio('http://localhost:4113');
//Vue.use(VueSocketIO, SocketInstance)

//Create the App with the router
new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app');